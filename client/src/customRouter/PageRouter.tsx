import React from "react";
import { useParams } from "react-router-dom";
import NotFound from "../components/NotFound";
import { IParams } from "../utils/types";

const generatePage = (pageName:string):JSX.Element => {
  const component = () => require(`../pages/${pageName}`).default;

  try {
    return React.createElement(component());
  } catch (error) {
    return <NotFound />;
  }
};

export default function PageRoute() {

  const {slug,page }:IParams = useParams();
  
  let pageName:string = "";
  
    if (page) {
      pageName =slug ?`${page}/[slug]`:`${page}`
    } 

  return generatePage(pageName);
}
