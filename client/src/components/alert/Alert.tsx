import React, { FC } from "react";
import { useSelector } from "react-redux";
import Loading from "./Loading";
import { TState } from "../../redux/reducers/index";
import Toast from "./Toast";

export const Alert: FC = () => {
  const { alert } = useSelector((state: TState) => state);

  return (
    <div>
      {alert.loading && <Loading />}
      {alert.errors &&
        <Toast bgColor="bg-danger" body={alert.errors} title="Errors" />}
      {alert.success &&
        <Toast bgColor="bg-success" body={alert.success} title="Success" />}
    </div>
  );
};

export const showErrorMsg = (msg: string) => {
  return (
    <div className="errMsg">
      {msg}
    </div>
  );
};

export const showSuccessrMsg = (msg: string) => {
  return (
    <div className="successMsg">
      <div className="success-checkmark">
        <div className="check-icon">
          <span className="icon-line line-tip" />
          <span className="icon-line line-long" />
          <div className="icon-circle" />
          <div className="icon-fix" />
        </div>
      </div>
      {msg}
    </div>
  );
};
