import React, { FC } from "react";
import { useDispatch } from "react-redux";
import { Alert } from "../../redux/types/alertTypes";

interface IState {
  title: string;
  body: string | string[];
  bgColor: string;
}

const Toast: FC<IState> = ({ title, body, bgColor }) => {
  const dispatch = useDispatch();

  const handelClick = () => {
    dispatch({
      type: Alert.ALERT,
      payload: {}
    });
  };

  return (
    <div
      className={`toast show position-fixed text-light ${bgColor}`}
      style={{
        top: "20px",
        right: "20px",
        zIndex: 50,
        minWidth: "200px"
      }}
    >
      <div className={`toast-header text-light ${bgColor}`}>
        {/* <img src="..." className="rounded me-2" alt="..." /> */}
        <strong className="me-auto">
          {title}
        </strong>
        <button
          type="button"
          className="btn-close"
          data-bs-dismiss="toast"
          aria-label="Close"
          onClick={handelClick}
        />
      </div>
      <div className="toast-body">
        {typeof body === "string"
          ? body
          : <ul>
              {body.map((text, index) =>
                <li key={index}>
                  {text}
                </li>
              )}
            </ul>}
      </div>
    </div>
  );
};

export default Toast;
