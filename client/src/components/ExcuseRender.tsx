import React, { FC, useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { bindActionCreators } from "redux";
import * as exucesCreator from "../redux/actions/exusesActions";
import * as alertCreator from "../redux/actions/alertAction";
import { TState } from "../redux/reducers";
import { IExProps } from "../redux/types/exuces.type";
import { randChoice } from "../utils/fetchData";
import ButtonRender from "./ButtonRender";
import ExcuseMessage from "./ExcuseMessage";

const ExcuseRender= () => {
  const { excuces } = useSelector((state: TState) => state);
  const [randomState, setRandomState] = useState<IExProps>();
  const [isRefresh, setIsRefresh] = useState<boolean>(true);
  
  const dispatch = useDispatch();
  const { filterExucesAction } = bindActionCreators(exucesCreator, dispatch);
  const { alertAction } = bindActionCreators(alertCreator, dispatch);

  useEffect(() => {
     setRandomState(randChoice<IExProps>(excuces?.excusesRandom))
  },[excuces])
  
  const hendelrenderItem = () => {
    if (excuces.excusesRandom.length > 0) {
      setIsRefresh(false)
      alertAction({loading:true})
      window.setTimeout(() => {
        randomState && filterExucesAction(randomState)
        setRandomState(randChoice<IExProps>(excuces.excusesRandom));
        alertAction({ loading: false })
        setIsRefresh(true)
        
      }, 500)
     }
  };

  return (
   <div className="home_page"> 
 <div className="excuse_component">
      <h6 className="excuse_title">Exuse message</h6>
        {
          isRefresh&&<> <ExcuseMessage randomState={randomState} />
         <ButtonRender hendelrenderItem={hendelrenderItem} randomState={randomState} />
          </>}
      </div>
    </div>
  );
};

export default ExcuseRender;
