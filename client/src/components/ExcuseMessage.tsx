import React from "react";
import { IExProps } from "../redux/types/exuces.type";
import logo from "../images/sorry.png";
interface IProps {
  randomState?: IExProps;
}

const ExcuseMessage: React.FC<IProps> = ({ randomState }) => {
  return (
    <div className="excuse_message">
      {randomState
        ? <p>
            {randomState.message}
          </p>
        : <img src={logo} alt="" title="excuse me?" />}
    </div>
  );
};

export default ExcuseMessage;
