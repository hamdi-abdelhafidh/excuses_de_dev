import React,{FC} from "react";
import { useDispatch } from "react-redux";
import { bindActionCreators } from "redux";
import * as excuseCreator from "../redux/actions/exusesActions";
import { IExProps } from "../redux/types/exuces.type";
import { ITagProps } from "../redux/types/tags.types";

interface IProps {
  excuces: IExProps[];
  tags: ITagProps[];
}

const ExcusesTable: FC<IProps> = ({ excuces, tags }) => {
    const dispatch = useDispatch()
    const {deleteExucesAction}= bindActionCreators(excuseCreator,dispatch)
   
    const handelDelete = (id:string):void => {
      if (window.confirm("Are you wont to delete this excise")) {
        deleteExucesAction(id)
      }
    }

  return (
    <div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">tag</th>
            <th scope="col">excuse mesage</th>
            <th scope="col">Actions</th>
          </tr>
        </thead>
              <tbody>
              {excuces?.map((item:IExProps,i:number) => <tr key={item._id}>
            <th scope="row">{i+1}</th>
            <td className="w-10">{item?.tag?.tag}</td>
            <td>{item?.message}</td>
             <td>
                    {/* <small className="mx-2  pointer btn btn-primary">update</small> */}
                    <small className="mx-1  pointer btn btn-danger" onClick={()=>{item._id && handelDelete(item._id)}}>x</small>
            </td>
          </tr>)}
          
        </tbody>
      </table>
    </div>
  );
};

export default ExcusesTable;
