import React, { FC, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { bindActionCreators } from "redux";
import * as alertCreator from "../../redux/actions/alertAction";
import * as excuseCreator from "../../redux/actions/exusesActions";
import { TState } from "../../redux/reducers";
import { IValidationErrorsState } from "../../utils/types";
import { excuseValidations } from "../../utils/validations";

export interface IProps {
  message: string;
  tag: string;
}
const initialState: IProps = {
  message: "",
  tag: ""
};
const Modal: FC = () => {
  const dispatch = useDispatch();
  const { alertAction } = bindActionCreators(alertCreator, dispatch);
  const { createExucesAction } = bindActionCreators(excuseCreator, dispatch);

  const { tags } = useSelector((state: TState) => state);
  const [formData, setFormData] = useState<IProps>(initialState);
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [errors, setErrors] = useState<IValidationErrorsState>();

  const handelChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>
  ) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handelSubmit = (
    e: React.FormEvent<HTMLFormElement | HTMLButtonElement>
  ) => {
    e.preventDefault();
    setIsOpen(false);
    const check = excuseValidations({ ...formData });

    if (check?.messErr || check?.tagErr) {
      return setErrors(check);
    }
    const tag = tags.find(item => item._id === formData.tag)

  tag &&  createExucesAction(formData.message, tag);
  };
  return (
    <div
      className="modal fade"
      id="exampleModal"
      tabIndex={1}
      aria-labelledby="exampleModalLabel"
      aria-hidden={isOpen}
    >
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">
              New Excuse
            </h5>
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            />
          </div>
          <div className="modal-body">
            <form onSubmit={handelSubmit}>
              <div className="mb-3">
                <label htmlFor="recipient-name" className="col-form-label">
                  Excuse message:
                </label>
                <input
                  placeholder="enter message"
                  type="text"
                  className="form-control"
                  id="recipient-name"
                  name="message"
                  value={formData.message}
                  onChange={handelChange}
                />
                {errors?.messErr&& <small className="text-danger">{errors?.messErr }</small>}
              </div>
              <div className="mb-3">
                <select
                  className="form-select"
                  aria-label="Default select example"
                  name="tag"
                  value={formData.tag}
                  onChange={handelChange}
                >
                  <option>Open this select menu</option>
                  {tags.length > 0 &&
                    tags.map(item =>
                      <option key={item._id} value={item._id}>
                        {item.tag}
                      </option>
                    )}
                </select>
               {errors?.tagErr&& <small className="text-danger">{errors?.tagErr }</small>}
              </div>
            </form>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary"
              data-bs-dismiss="modal"
            >
              Close
            </button>
            <button
              type="submit"
              className="btn btn-primary"
              data-bs-dismiss={
                !formData.message || !formData.tag ? "" : "modal"
              }
              onClick={handelSubmit}
            >
              Send message
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Modal;
