import React from "react";
import { Link } from "react-router-dom";

const Header = () => {
  return (
    <div className="nav header">
      <ul>
        <li>
          <Link to={"/"}>Home</Link>
        </li>
        <li>
          <Link to={"/excuses"}>Excuses</Link>
        </li>
      </ul>
    </div>
  );
};

export default Header;
