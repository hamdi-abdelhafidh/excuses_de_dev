import React, { FC } from "react";
import { IExProps } from "../redux/types/exuces.type";

interface IProps {
  randomState?: IExProps;
  hendelrenderItem: () => void;
}

const ButtonRender: FC<IProps> = ({ hendelrenderItem, randomState }) => {
  return (
    <button
      style={{ cursor: randomState ? "pointer" : "no-drop" }}
      className="excuse_buttont"
      onClick={hendelrenderItem}
    >
      new message
    </button>
  );
};

export default ButtonRender;
