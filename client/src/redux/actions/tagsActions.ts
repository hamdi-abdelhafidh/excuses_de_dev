import { Dispatch } from "redux"
import { fetchAPI } from "../../utils/fetchData"
import { Alert, IAlertActions } from "../types/alertTypes"
import { TagTypes,  TTagActions } from "../types/tags.types"


export const getTagsAction = () => async (dispatch:Dispatch<TTagActions|IAlertActions>) => {
    try {
        const res = await fetchAPI(`tags`)
        dispatch({
            type: TagTypes.FETCH_TAGS,
            payload:res.data.data
        })

    } catch (error:any) {
        dispatch({
            type: Alert.ALERT,
            payload: {
                errors:error.response?.data.msg
            }
        })
    }
}