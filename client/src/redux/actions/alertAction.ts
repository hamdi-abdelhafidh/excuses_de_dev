import { Dispatch } from "redux";

import { Alert, IAlert, IAlertActions } from "../types/alertTypes";

export const alertAction = (payload:IAlert) => async (dispatch: Dispatch<IAlertActions>) => {
    dispatch({
        type: Alert.ALERT,
        payload
    })
}