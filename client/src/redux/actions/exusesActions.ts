import { Dispatch } from "redux"
import { deleteAPI, fetchAPI, postAPI } from "../../utils/fetchData"
import { ExucesTypes, IExProps, TExcActions } from "../types/exuces.type"
import { IAlertActions,Alert } from "../types/alertTypes";
import { ITagProps } from "../types/tags.types";


export const getExucesAction = () => async (dispatch:Dispatch<TExcActions|IAlertActions>) => {
    try {
        const res = await fetchAPI(`excuses`)
        dispatch({
            type: ExucesTypes.FETCH_EXUCES,
            payload:res.data.data.excuses
        })
    } catch (error:any) {
        dispatch({
            type: Alert.ALERT,
            payload: {
                errors:error.response?.data.msg
            }
        })
    }
}

export const filterExucesAction = (data:IExProps) => async (dispatch:Dispatch<TExcActions|IAlertActions>) => {
    try {

        // console.log("refrech")
        dispatch({
            type: ExucesTypes.FILTER_EXUSES,
            payload:data
        })
    } catch (error:any) {
        dispatch({
            type: Alert.ALERT,
            payload: {
                errors:error.response?.data.msg
            }
        })
    }
}

export const createExucesAction = (message:string,tag:ITagProps) => async (dispatch:Dispatch<TExcActions|IAlertActions>) => {
    try {
        const res= await postAPI(`excuses`,{message,tag})
        dispatch({
            type: ExucesTypes.CREATE_EXUSE,
            payload: {
                ...res.data.data, tag
            }
        })
        dispatch({
            type: Alert.ALERT,
            payload: {
                success:"Create New excuse message with success"
            }
        })
    } catch (error:any) {
        dispatch({
            type: Alert.ALERT,
            payload: {
                errors:error.response?.data.msg
            }
        })
    }
}

export const deleteExucesAction = (id:string) => async (dispatch:Dispatch<TExcActions|IAlertActions>) => {
    try {
        dispatch({
            type: ExucesTypes.DELETE_EXUSE,
            payload: {_id:id}
        })
        
        await deleteAPI(`excuses/${id}`)
        
        
    } catch (error:any) {
        dispatch({
            type: Alert.ALERT,
            payload: {
                errors:error.response?.data.msg
            }
        })
    }
}
