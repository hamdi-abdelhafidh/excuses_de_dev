import { Dispatch } from "redux";
import { IExProps } from "../types/exuces.type";
import { TModalActions, ModalTypes } from "../types/modalTypes";

export const modalAction = (data: IExProps) => async (
  dispatch: Dispatch<TModalActions>
) => {
  console.log({ data });
  dispatch({
    type: ModalTypes.MODAL,
    payload: data
  });
};
