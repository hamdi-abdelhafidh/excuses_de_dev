import { ExucesTypes,TExcActions,IExProps } from "../types/exuces.type";

interface IExcuseReducersProps{
    excusesItem: IExProps[] ,
    excusesRandom:IExProps[] 
}
const initialState: IExcuseReducersProps = {
    excusesItem:[],
    excusesRandom:[]
}

const excusesReducers = (state:IExcuseReducersProps=initialState, action:TExcActions):IExcuseReducersProps => {
    switch (action.type) {
        case ExucesTypes.FETCH_EXUCES:
            return {
                ...state,
                excusesItem:action.payload,
                excusesRandom:action.payload,
            }
        case ExucesTypes.FILTER_EXUSES:
            return {
                ...state,
                excusesRandom: state.excusesRandom.filter(exc => exc._id !== action.payload._id)
            }
            case ExucesTypes.CREATE_EXUSE:
            return {
                ...state,
                excusesItem:[...state.excusesItem,action.payload],
                excusesRandom:[...state.excusesRandom,action.payload],
            }
            
            case ExucesTypes.DELETE_EXUSE:
            return {
                ...state,
                excusesItem:state.excusesItem.filter(item=>item._id !==action.payload._id),
                excusesRandom:state.excusesRandom.filter(item=>item._id !==action.payload._id),
            }
        
        default:
            return state;
    }
}

export default excusesReducers