import { Alert, IAlertActions ,IAlert} from "../types/alertTypes";



const alertReducer = (state:IAlert = {}, action:IAlertActions):IAlert => {
    switch (action.type) {
        case Alert.ALERT:
            return action.payload
        default:
           return state
    }
}


export default alertReducer