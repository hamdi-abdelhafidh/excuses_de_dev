import { ITagProps, TagTypes, TTagActions } from "../types/tags.types";


const tagsReducers = (state:ITagProps[]=[], action:TTagActions):ITagProps[] => {
    switch (action.type) {
        case TagTypes.FETCH_TAGS:
            return action.payload
    
        default:
            return state;
    }
}

export default tagsReducers