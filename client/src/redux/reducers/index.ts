import { combineReducers } from "redux";
import tagsProps from "./tagsReducer";
import excusesProps from "./exucesReducers"
import alertReducer from "./alertReducer"
import modalReducer from "./modalReducer"
const reducers =  combineReducers({
  tags:tagsProps,
  excuces: excusesProps,
  alert:alertReducer,
  modal:modalReducer
});
export default reducers

export type TState = ReturnType<typeof reducers>