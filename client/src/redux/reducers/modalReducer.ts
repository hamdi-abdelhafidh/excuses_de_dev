import { IExProps } from "../types/exuces.type";
import { ModalTypes, TModalActions } from "../types/modalTypes";


interface IProps{
 global:IExProps|null
}
const initilaState:IProps = {
    global:null
}
 const modalReducer = (state:IProps["global"] = initilaState.global ,action:TModalActions):IProps["global"] => {
    switch (action.type) {
        case ModalTypes.MODAL:
            return action.payload
    
        default:
            return state;
    }
}

export default modalReducer