
export interface ITagProps{
    _id: string
    tag: string,
}


export enum TagTypes{
FETCH_TAGS="FETCH_TAGS",
CREATE_TAG="CREATE_TAG"
}

export type TFetchTags={
    type: TagTypes.FETCH_TAGS
    payload: ITagProps[]
}

export type TCreateTag={
    type: TagTypes.CREATE_TAG
    payload:ITagProps
}

export type TTagActions =  TFetchTags | TCreateTag