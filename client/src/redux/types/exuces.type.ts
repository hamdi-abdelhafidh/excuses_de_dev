import { ITagProps } from "./tags.types"

export interface IExProps{
    _id?: string,
    message?: string,
    tag?: ITagProps,
}


export enum ExucesTypes{
FETCH_EXUCES="FETCH_EXUCES",
FETCH_EXUSE="FETCH_EXUSE",
CREATE_EXUSE="CREATE_EXUSE",
UPDATE_EXUSE="UPDATE_EXUSE",
FILTER_EXUSES="FILTER_EXUSES",
DELETE_EXUSE="DELETE_EXUSE",
}

export type TFetchExcsAction={
    type: ExucesTypes.FETCH_EXUCES
    payload: IExProps[]
}

export type TFetchExcAction={
    type: ExucesTypes.FETCH_EXUSE
    payload: IExProps
}

export type TCreateExcAction={
    type: ExucesTypes.CREATE_EXUSE
    payload: IExProps
}

export type TUpdateExcAction={
    type: ExucesTypes.UPDATE_EXUSE
    payload: IExProps
}


export type TFilterExcsAction={
    type: ExucesTypes.FILTER_EXUSES
    payload: IExProps
}

export type TDeleteExcAction={
    type: ExucesTypes.DELETE_EXUSE
    payload: IExProps
}

export type TExcActions =  TFetchExcsAction |TFetchExcAction|TCreateExcAction | TDeleteExcAction| TUpdateExcAction| TFilterExcsAction