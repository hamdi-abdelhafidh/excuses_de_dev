import { IExProps } from "./exuces.type";


export enum ModalTypes{
    MODAL="MODAL"
}

    
export interface TModalActions {
    type: ModalTypes.MODAL,
    payload:IExProps | null 
}