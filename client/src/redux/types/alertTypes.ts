
export interface IAlert{
    loading?: boolean
    success?:string | string[]
    errors?:string | string[]
}

export enum Alert{
    ALERT = "ALERT"
}

export interface IAlertType{
    type: Alert.ALERT,
    payload:IAlert
}


export type IAlertActions = IAlertType



