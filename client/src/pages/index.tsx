import React from "react";
import ExcuseRender from "../components/ExcuseRender";

const Home = () => {
  return (
    <div className="main">
      <ExcuseRender />
    </div>
  );
};

export default Home;
