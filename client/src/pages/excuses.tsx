import React from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import ExcusesTable from "../components/ExcusesTable";
import { TState } from "../redux/reducers";

const Excuses = () => {
  const { excuces, tags } = useSelector((state: TState) => state);
  const navigation = useNavigate();
  return (
    <div className="excuse_page">
      <div className="render_excuses ">
        <div className="d-flex justify-content-end">
          <button
            type="button"
            className="btn btn btn-secondary"
            data-bs-toggle="modal"
            data-bs-target="#exampleModal"
            data-bs-whatever="@mdo"
          >
            New excuse
          </button>
          <button
            className="btn btn-dark mx-2 pointer"
            onClick={() => navigation("/")}
          >
            Back
          </button>
        </div>
        <ExcusesTable tags={tags} excuces={excuces.excusesItem} />
      </div>
    </div>
  );
};

export default Excuses;
