import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { bindActionCreators } from "redux";
import * as actionTagCreators from "./redux/actions/tagsActions";
import * as actionExcCreators from "./redux/actions/exusesActions";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import PageRoute from "./customRouter/PageRouter";
import Header from "./components/Header";
import { Alert } from "./components/alert/Alert";
import Modal from "./components/modal/Modal";
import { TState } from "./redux/reducers";
function App() {
  const { modal } = useSelector((state: TState) => state);
  const dispatch = useDispatch();
  const { getTagsAction } = bindActionCreators(actionTagCreators, dispatch);
  const { getExucesAction } = bindActionCreators(actionExcCreators, dispatch);

  useEffect(
    () => {
      getTagsAction();
      getExucesAction();
    },
    [dispatch]
  );

  return (
    <div className="APP">
      <Alert />
      <Modal />
      <Router>
        <Header />
        <Routes>
          <Route path="/" element={<PageRoute />} />
          <Route path=":page" element={<PageRoute />} />
          <Route path=":page/:slug" element={<PageRoute />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
