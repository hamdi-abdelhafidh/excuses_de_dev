import { IValidationErrorsState } from "./types";

interface IProps {
  message: string;
  tag: string;
}

export const excuseValidations = ({
  message,
  tag
}: IProps): IValidationErrorsState => {
  const err: IValidationErrorsState = {};
  if (!message) {
    err.messErr = "Message is required";
  }
  if (!tag) {
    err.tagErr = "Tag is required";
  }

  return { ...err };
};
