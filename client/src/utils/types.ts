import { ITagProps } from "../redux/types/tags.types";

export interface IParams {
  page?: string;
  slug?: string;
}

export interface IValidationErrorsState {
  messErr?: string;
  tagErr?: string;
}

export interface IExMessageProps {
  message: string;
  tag: ITagProps;
}
