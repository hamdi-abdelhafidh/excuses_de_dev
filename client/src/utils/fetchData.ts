import axios from "axios";
import { RESTFULL_API } from "./config";

export const fetchAPI = async (url: string) => {
  const res = await axios.get(`${RESTFULL_API}${url}`);
  return res;
};

export const postAPI = async (url: string, body: {}) => {
  const res = await axios.post(`${RESTFULL_API}${url}`, body);
  return res;
};

export const deleteAPI = async (url: string) => {
  const res = await axios.delete(`${RESTFULL_API}${url}`);
  return res;
};

export function randChoice<T>(arr: Array<T>): T {
  const rand = arr[Math.floor(Math.random() * arr.length)];
  console.log({ rand });
  return rand;
}
