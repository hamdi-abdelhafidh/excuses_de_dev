1. Build a image.
- docker build -t "image-name" .
- docker build -f "Dockerfile.dev" -t "image-name" .

2. Run a container.
- docker run -it --rm --name "container-name" -p 5000:5000 "image-name"
- docker run -it -d --rm --name "container-name" --env-file .env -p 5000:5000 "image-name"
- docker run -it -d --rm --name "container-name" -p 5000:5000 "image-name"
- with -d: detach -> run a container in the background.

3. Stop a container.
- docker stop "container-name"

4. Stop all running containers.
- docker stop $(docker ps -a -q)
doc
5. Show all images.
- docker images

6. Delete a image.
- docker rmi "image-id|image-name"

7. Delete all images.
- docker rmi $(docker images -q)

8. Delete all <none> images.
- docker rmi $(docker images -f dangling=true -q)

9. Show all running containers.
- docker ps

10. Show all containers.
- docker ps -a

11. Delete a stopped container.
- docker rm "container-name"

12. Delete all stopped containers.
- docker rm $(docker ps -a -q)

13. Kill all running containers.
- docker kill $(docker ps -q)

14. List volumes.
- docker volume ls

15. Remove all unused local volumes.
- docker volume prune

16. Remove one or more volumes.
- docker volume rm "volume-name"

17. Run commands in a docker container.
- docker exec -it "container-name" sh

18. Push image to docker hub.
- docker push "image-name:tag"

19. Pull image from docker hub.
- docker pull "image-name:tag"
19. connect docker hub.
- username:
- password:  


## Commonly used commands in docker-compose:
1. Build and rebuild a image.
- docker-compose up --build
- docker-compose -f "docker-compose.dev.yml" up --build
- docker-compose -f docker-compose.yml up -d --build api-dev
- docker-compose -f "docker-compose.dev.yml" up --build -d
- docker-compose -f "docker-compose.dev.yml"up api-dev

2. Run and start containers.
- docker-compose up
- docker-compose -f "docker-compose.dev.yml" up
- docker-compose -f "docker-compose.dev.yml" up    api-dev
3. Stop and clear containers.
- docker-compose down
- docker-compose -f "docker-compose.dev.yml" down

4. Stop and clear containers, volumes.
- docker-compose down -v
- docker-compose -f "docker-compose.dev.yml" down -v



### III. Setup gitlab-runner:  
  1. Download the binary for your system   
    - `sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64`  

  2. Give it permissions to execute       
    - `sudo chmod +x /usr/local/bin/gitlab-runner`        
             
  3. Create a GitLab CI user  
    - `sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash`      

  4. Install and run as service  
    - `sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner`          
    - `sudo gitlab-runner start`          

  5. Register a runner   
    - `sudo gitlab-runner register --url https://gitlab.com/ --registration-token "registration_token"`  

  6. Add gitlab-runner to the docker group       
    - `sudo usermod -aG docker gitlab-runner`     
  
  7. Grant sudo permissions to the gitlab-runner user.              
    - `sudo nano /etc/sudoers`        
    - `gitlab-runner ALL=(ALL) NOPASSWD: ALL`      

  8. Fix bug.             
    - `sudo nano /home/gitlab-runner/.bash_logout`       
