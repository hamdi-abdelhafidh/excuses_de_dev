import { Request, Response, NextFunction } from "express";

export const valitadeTag = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { tag } = req.body;
  if (!tag) {
    return res.status(400).json({ msg: "Please add tag.!" });
  }

  next();
};

export const valitadeExcuse = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { message, tag } = req.body;
  if (!tag) {
    return res.status(400).json({ msg: "Please add tag.!" });
  }

  if (!message) {
    return res.status(400).json({ msg: "Please add message.!" });
  }

  next();
};
