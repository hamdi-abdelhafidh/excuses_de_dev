import { query, Request, Response } from "express";
import Excuses from "../models/excuseModel";
import { featuresAPI } from "../lib/featuresApi";
const excuseCtrl = {
  getALLExcuses: async (req: Request, res: Response) => {
    try {
      const features = new featuresAPI(Excuses.find(), req.query)
        .paginating()
        .sorting()
        .searching()
        .filtering();
      const result = await Promise.allSettled([
        features.query.populate("tag"),
        Excuses.countDocuments(),
      ]);
      const excuses = result[0].status === "fulfilled" ? result[0].value : [];
      const count = result[1].status === "fulfilled" ? result[1].value : 0;

      res.json({
        status: "Ok",
        msg: "success",
        resultLength: excuses.length,
        count,
        data: {
          excuses,
        },
      });
    } catch (error) {
      return res.status(500).json({ msg: (error as Error).message });
    }
  },
  getOneExcuse: async (req: Request, res: Response) => {
    try {
      const excuse = await Excuses.findById(req.params.id).populate(
        "tag",
        "tag"
      );
      if (!excuse) {
        return res.status(404).json({
          msg: "Excuse message not found",
        });
      }
      res.json({
        status: "Ok",
        msg: "success",
        data: excuse,
      });
    } catch (error) {
      if (error.kind === "ObjectId") {
        return res.status(404).json({ msg: "Excuse message not found" });
      }
      return res.status(500).json({ msg: (error as Error).message });
    }
  },
  createNewExcuse: async (req: Request, res: Response) => {
    try {
      const { message, tag } = req.body;

      let msg = await Excuses.findOne({ message });
      if (msg) return res.status(400).json({ msg: "this msg already exist" });

      const newExcuse = new Excuses({
        message,
        tag,
      });
      await newExcuse.save();
      res.json({
        status: "Ok",
        msg: "success",
        data: newExcuse,
      });
    } catch (error) {
      return res.status(500).json({ msg: (error as Error).message });
    }
  },
  deleteExcuse: async (req: Request, res: Response) => {
    try {
      await Excuses.findOneAndDelete({
       _id:req.params.id
     })
      res.json({
        status: "Ok",
        msg: "success",
      });
    } catch (error) {
      return res.status(500).json({ msg: (error as Error).message });
    }
  },
};

export default excuseCtrl;
