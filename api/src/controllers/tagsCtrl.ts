import { Request, Response } from "express";
import Tags from "../models/tagModel";
const tagsCtrl = {
  getALLTags: async (req: Request, res: Response) => {
    try {
      const tags = await Tags.find();
      res.json({
        status: "Ok",
        msg: "success",
        data: tags,
      });
    } catch (error) {
      return res.status(500).json({ msg: (error as Error).message });
    }
  },

  createNewTags: async (req: Request, res: Response) => {
    try {
      const { tag } = req.body;

      let tags = await Tags.findOne({ tag });
      if (tags) return res.status(400).json({ msg: "this tags already exist" });

      const newtag = new Tags({
        tag,
      });
      await newtag.save();
      res.json({
        status: "Ok",
        msg: "success",
        data: newtag,
      });
    } catch (error) {
      return res.status(500).json({ msg: (error as Error).message });
    }
  },
};

export default tagsCtrl;
