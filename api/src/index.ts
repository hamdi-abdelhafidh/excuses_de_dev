// const n: string = "hello";
// console.log(n);
import dotenv from "dotenv";
dotenv.config();
import express, { application } from "express";
import mongoose from "mongoose";
import cors from "cors";
import morgan from "morgan";
import path from "path";
import routes from "./routes/index";
const app = express();

// middlware
app.use(cors());
app.use(morgan("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// database
import "./config/database";

// routes
app.get("/", (req, res) => {
  res.json({ mg: "hello test react/node " });
});
app.use("/api", routes);

// start server listening
const PORT = process.env.PORT || 5000;

app.listen(PORT, () => {
  console.log(`server is running in : ${PORT}`);
});
