import mongoose from "mongoose";
const URL = process.env.MONGODB_URL;
mongoose.connect(
  `${URL}`,
  {
    autoIndex: false
  },
  err => {
    if (err) throw err;
    console.log("MongoDB connected..!");
  }
);
