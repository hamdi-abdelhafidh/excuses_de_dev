import express from "express";
import tagsCtrl from "../controllers/tagsCtrl";
import { valitadeTag } from "../middlewares/validates";
const router = express.Router();

// get all tags
router.get("/tags", tagsCtrl.getALLTags);

// create new tag
router.post("/tag", valitadeTag, tagsCtrl.createNewTags);

export default router;
