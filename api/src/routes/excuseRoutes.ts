import express from "express";
import excuseCtrl from "../controllers/excuseCtrl";
import { valitadeExcuse } from "../middlewares/validates";
const router = express.Router();

//  get all excuses
router.get("/excuses", excuseCtrl.getALLExcuses);

//  get one excuses by id
router.get("/excuses/:id", excuseCtrl.getOneExcuse);

//  create new excuse
router.post("/excuses", valitadeExcuse, excuseCtrl.createNewExcuse);

//  delete excuse
router.delete("/excuses/:id", excuseCtrl.deleteExcuse);

export default router;
