// import all routes
import excuseRoutes from "./excuseRoutes";
import tagsRoutes from "./tagsRoutes";

const routes = [excuseRoutes, tagsRoutes];

export default routes;
