import mongoose from "mongoose";

const excuseSchema = new mongoose.Schema(
  {
    message: {
      type: String,
      required: true,
      unique: true,
    },
    tag: {
      type: mongoose.Types.ObjectId,
      ref: "Tag",
    },
  },
  { timestamps: true }
);

export default mongoose.model("Excuse", excuseSchema);
